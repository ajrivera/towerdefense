﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Manager : MonoBehaviour
{
    // delegat
    private delegate void upgrade();
    private upgrade upgrade_lvl;

    private GameObject myManager;
    public GameObject Sprite;
    // nivell actual de la torre.
    private int Avtual_Lvl = 1;
    // num dispars de la torre
    private float Cadencia = 1;
    // enemic mor de 2 dispars
    private int dmg = 10;
    // enemic al que dispara
    public GameObject Enemy;
    // indica si la torre pot disparar o no
    private bool can_shoot = true;
    // altura de la torre per a instanciar la bala al disparar
    private float height;
    private GameObject target = null;
 
    private bool air = false;



    private void Start()
    {
        myManager = GameObject.FindGameObjectWithTag("GameManager");
        this.height = this.GetComponentInChildren<GroundChangeSprite>().GetSpriteHeight();
        this.upgrade_lvl +=  this.GetComponent<Manager>().lvlUp;
        this.upgrade_lvl +=  this.Sprite.GetComponent<GroundChangeSprite>().SpriteUpdate;
    }
    // Start is called before the first frame update
    private void Update()
    {
      
       if (Input.GetKeyDown(KeyCode.U))
        {
            print(this.Avtual_Lvl);
            this.Update_Tower(this.GetCanUpgrade());
        }
    }

    public bool GetIsAir ()
    {
        return this.air;
    }

    public void SetIsAir()
    {
        this.air = true;
    }
    public void PrepareNextShoot()
    {
        Invoke("ShootToTrue",this.Cadencia);
    }

    public int GetActualLvl()
    {
        return this.Avtual_Lvl;
    }

    private void lvlUp()
    {
        this.Avtual_Lvl++;
        this.dmg += 2;
        this.Cadencia -= 0.2f;
        this.height = this.GetComponentInChildren<GroundChangeSprite>().GetSpriteHeight();
    }

    public Boolean GetCanUpgrade()
    {
        if (this.Avtual_Lvl == 5)
            return false;
        
        return true;
    }


    public void Update_Tower(Boolean b)
    {
        if (this.Avtual_Lvl == 1 && myManager.GetComponent<GameManager>().gold >= 75) {
            if (b)
            {
                print("tower updated");
                myManager.GetComponent<GameManager>().gold -= 75;
                this.upgrade_lvl();
            }
        }
        else if (this.Avtual_Lvl == 2 && myManager.GetComponent<GameManager>().gold >= 100 && myManager.GetComponent<GameManager>().iron >= 20)
        {
            if (b)
            {
                print("tower updated");
                myManager.GetComponent<GameManager>().gold -= 100;
                myManager.GetComponent<GameManager>().iron -= 20;
                this.upgrade_lvl();
            }
        }
        else if (this.Avtual_Lvl == 3 && myManager.GetComponent<GameManager>().gold >= 150 && myManager.GetComponent<GameManager>().iron >= 40)
        {
            if (b)
            {
                print("tower updated");
                myManager.GetComponent<GameManager>().gold -= 150;
                myManager.GetComponent<GameManager>().iron -= 40;
                this.upgrade_lvl();
            }
        }
        else if (this.Avtual_Lvl == 4 && myManager.GetComponent<GameManager>().gold >= 230 && myManager.GetComponent<GameManager>().iron >= 70)
        {
            if (b)
            {
                print("tower updated");
                myManager.GetComponent<GameManager>().gold -= 230;
                myManager.GetComponent<GameManager>().iron -= 70;
                this.upgrade_lvl();
            }
        }
        else
        {
            myManager.GetComponent<GameManager>().goldgui.color = Color.red;
            myManager.GetComponent<GameManager>().irongui.color = Color.red;
            Invoke("returnColorGold", 1.5f);
            Invoke("returnColorIron", 1.5f);
        }
    }

    private void returnColorGold()
    {
        myManager.GetComponent<GameManager>().goldgui.color = Color.yellow;
    }

    private void returnColorIron()
    {
        myManager.GetComponent<GameManager>().irongui.color = Color.yellow;
    }


    public int GetDmg()
    {
        return this.dmg;
    }

    public GameObject GetTarget()
    {
        return this.target;
    }


    public void AddTarget(GameObject go)
    {
        this.target = go;
    }

    public void AddTarget2(GameObject go)
    {
        this.target = go;
    }


    public GameObject GetTower()
    {
        return this.gameObject;
    }

    public GameObject GetEnemy()
    {
        return this.Enemy;
    }

    private void ShootToTrue()
    {
      //  print("puc disparar");
      //  print(this.can_shoot);
        this.can_shoot = true;
    }

    public bool GetCanShoot()
    {
        return can_shoot;
    }

    public void SetCanShootFalse()
    {
        this.can_shoot = false;
    }

    public void SetHeight(float f)
    {
        this.height = f;   
    }

    public float GetHeight()
    {
        return this.height;
    }
}
