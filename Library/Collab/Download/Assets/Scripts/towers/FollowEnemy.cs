﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using UnityEngine;

public class FollowEnemy : MonoBehaviour
{
    private static int vel = 10;
    private GameObject Enemy;
    public  GameObject Tower;


    // Update is called once per frame
    void Update()
    {  
        try
        {
            this.transform.position = Vector2.MoveTowards(this.transform.position, this.Enemy.transform.position, vel * Time.deltaTime);
        } catch (System.Exception e)
        {
            Destroy(this.gameObject);
        } 
    }

    public void SetTarget(GameObject go, GameObject to)
    {
        this.Enemy = go;
        this.Tower = to;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == this.Enemy.gameObject)
        {
            if (this.Tower.GetComponent<Manager>().GetIsAir())
            {
                //resta vida al enemic aeri
                this.Enemy.GetComponent<enemyb>().RestarVida(this.Tower.GetComponent<Manager>().GetDmg());
                Destroy(this.gameObject);
            } else
            {
                //resta vida al enemic terrestre
                this.Enemy.GetComponent<enemya>().RestarVida(this.Tower.GetComponent<Manager>().GetDmg());
                Destroy(this.gameObject);
            }

        }
    }

}
