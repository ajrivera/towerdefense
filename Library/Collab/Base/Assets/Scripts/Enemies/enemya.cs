﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class enemya : MonoBehaviour
{
    private int hpmax = 30;
    private int hp = 30;
    public float spd;
    private EnemyPath Wpoints;

    private int waypointIndex;

    public Image hpbar;

    private Transform childaux;

    private Animator animator;
    private int directx;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetInteger("state", 5);

        GameObject[] Wpointsarr;
        Wpointsarr = GameObject.FindGameObjectsWithTag("Waypoints");
        //InvokeRepeating("minvida",0,2f);
        Wpoints = Wpointsarr[Random.Range(0, Wpointsarr.Length)].GetComponent<EnemyPath>();
        this.transform.position = Wpoints.transform.position;
        childaux = Wpoints.waypoints[waypointIndex].GetChild(Random.Range(0, Wpoints.waypoints[waypointIndex].childCount)).transform;


        //state machine
        Vector2 ani = new Vector2(this.transform.position.x - childaux.position.x, this.transform.position.y - childaux.position.y);
        if ((ani.x < 0.2 && ani.x > -0.2) && ani.y < 0)
        {
            animator.SetInteger("state", 1);
        }
        else if ((ani.x < 0.2 && ani.x > -0.2) && ani.y > 0)
        {
            animator.SetInteger("state", 0);
        }
        else if ((ani.y < 0.2 && ani.y > -0.2) && ani.x < 0)
        {
            animator.SetInteger("state", 3);
        }
        else if ((ani.y < 0.2 && ani.y > -0.2) && ani.x > 0)
        {
            animator.SetInteger("state", 2);
        }
        else if (ani.x < 0 && ani.y < 0)
        {
            animator.SetInteger("state", 4);
        }
        else if (ani.x > 0 && ani.y < 0)
        {
            animator.SetInteger("state", 5);
        }
        else if (ani.x < 0 && ani.y > 0)
        {
            animator.SetInteger("state", 6);
        }
        else
        {
            animator.SetInteger("state", 7);
        }

    }

    // Update is called once per frame
    void Update()
    {
        hpbar.fillAmount =(float) ((hp*3f)/100f);
        if(hp<=0)
        {
            Invoke("death", 1f);
            animator.SetInteger("state", 8);
        }
        else
        {
            if (Wpoints.waypoints.Length != waypointIndex)
            {

                transform.position = Vector2.MoveTowards(transform.position, childaux.position, spd * Time.deltaTime);

                if (Vector2.Distance(transform.position, childaux.position) < 0.1f)
                {

                    waypointIndex++;
                    if (waypointIndex != Wpoints.waypoints.Length)
                    {
                        //print(waypointIndex);
                        childaux = Wpoints.waypoints[waypointIndex].GetChild(Random.Range(0, Wpoints.waypoints[waypointIndex].childCount)).transform;

                        //state machine
                        Vector2 ani = new Vector2(this.transform.position.x - childaux.position.x, this.transform.position.y - childaux.position.y);

                        if ((ani.x < 0.2 && ani.x > -0.2) && ani.y < 0)
                        {
                            animator.SetInteger("state", 1);
                        }
                        else if ((ani.x < 0.2 && ani.x > -0.2) && ani.y > 0)
                        {
                            animator.SetInteger("state", 0);
                        }
                        else if ((ani.y < 0.2 && ani.y > -0.2) && ani.x < 0)
                        {
                            animator.SetInteger("state", 3);
                        }
                        else if ((ani.y < 0.2 && ani.y > -0.2) && ani.x > 0)
                        {
                            animator.SetInteger("state", 2);
                        }
                        else if (ani.x < 0 && ani.y < 0)
                        {
                            animator.SetInteger("state", 4);
                        }
                        else if (ani.x > 0 && ani.y < 0)
                        {
                            animator.SetInteger("state", 5);
                        }
                        else if (ani.x < 0 && ani.y > 0)
                        {
                            animator.SetInteger("state", 6);
                        }
                        else
                        {
                            animator.SetInteger("state", 7);
                        }
                    }

                }
            }
            else
            {
                GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().minhp();
                Destroy(this.gameObject);
            }
        }
        
        //comprobar que escena esta
        //if(SceneManager.GetActiveScene().name=="Gameplay")

        //print("hola");
        //mientras queden waypoints por recorrer, movetowards
        //print("hola");
        

        

    }

    public void maxhpwaves(){
        hpmax+=15;
        hp=hpmax;
    }

    public void death()
    {
        Destroy(this.gameObject);
    }


    //prueba de perder vida y comprobar hpbar
    public void minvida(){
        //if(hp < hpmax){
        //}
        hp-=3;
        print(hp);
        print(hpbar.fillAmount);
    }

    public void RestarVida(int i)
    {
        this.hp -= i;
    }


}
