﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

public class Shoot_Air : MonoBehaviour
{
    public GameObject Bala;
    private GameObject Enemy;


    // Start is called before the first frame update
    void Start()
    {
        //this.Enemy = ;
        this.Enemy = this.GetComponentInParent<Manager>().GetEnemy();
        print(this.Enemy.transform.tag);
        print(this.GetComponentInParent<Manager>().GetEnemy().transform.tag);
        this.GetComponentInParent<Manager>().SetIsAir();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == this.Enemy.transform.tag)
        {
            // assigna el target1
            if (this.GetComponentInParent<Manager>().GetTarget() == null
                && collision.GetComponent<enemyb>().GetHp() > 0)
            {
                this.GetComponentInParent<Manager>().AddTarget(collision.gameObject);
                print("enemic assignat");
            }
        }

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == this.Enemy.transform.tag)
        {
            // assigna el target
            if (this.GetComponentInParent<Manager>().GetTarget() == null
                && collision.GetComponent<enemyb>().GetHp() > 0)
            {
                this.GetComponentInParent<Manager>().AddTarget(collision.gameObject);
                print("enemic assignat2");
            }
        }
        // borra target si aquest esta morint
        if (this.GetComponentInParent<Manager>().GetTarget() != null && this.GetComponentInParent<Manager>().GetTarget().GetComponent<enemyb>().GetHp() <= 0)
        {
            print("puta merda");
            this.GetComponentInParent<Manager>().AddTarget(null);
        }
        if (this.GetComponentInParent<Manager>().GetCanShoot() && collision.transform.tag == this.Enemy.transform.tag)
        {
            print("uwu");
            this.GetComponentInParent<Manager>().PrepareNextShoot();
            // Dispara al target
            Instantiate(Bala, new Vector3(this.GetComponentInParent<Transform>().position.x, this.GetComponentInParent<Transform>().position.y + this.GetComponentInParent<Manager>().GetHeight(),
                -1), Quaternion.identity).GetComponent<FollowEnemy>().SetTarget(this.GetComponentInParent<Manager>().GetTarget(), this.GetComponentInParent<Manager>().GetTower());
            this.GetComponentInParent<Manager>().SetCanShootFalse();
            print("puto otako");

        }
    }




    /**
     * cuan el target surt del rango de dispar borra el target
     */
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (this.GetComponentInParent<Manager>().GetTarget() != null && collision.gameObject == this.GetComponentInParent<Manager>().GetTarget())
        {
            this.GetComponentInParent<Manager>().AddTarget(null);
        }
    }

}