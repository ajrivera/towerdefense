# Tower Defense
## Resumen:
Nuestro grupo ha decidido desarrollar un TowerDefense con vista isométrica, hemos decidido implementar dos tipos de enemigos(voladores y de tierra), a su vez hemos implementado dos torretas para cada tipo de enemigo. El juego consiste en dos niveles.

## Jugabilidad:
El juego contiene una pestaña de tienda donde se pueden construir torretas al lado de los caminos por donde pasan los enemigos, a su vez las torretas implementan un sistema de niveles para que se puedan mejorar y mejore su ataque y velocidad de disparo. Los enemigos son cada vez más difíciles.

## Ampliaciones:
-Tenemos 4 escenas: Inicio,1r Nivel,2o Nivel y GameOver. <br>
-Animaciones en los enemigos. <br>
-Interfaces avanzadas, con elementos extras: HPbar de los enemigos, Cambio del Cursor, ... <br>
-Acceso a componentes: Scripts, TileMap, Colliders, Componentes del hijo, SpriteRenderer, ... <br>
-Dentro de la grid hay un tilemap en específico que no permite construir torretas. <br>
-Musica.<br><br>

-El ivan es retrasado.