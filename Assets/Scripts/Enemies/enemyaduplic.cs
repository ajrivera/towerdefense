﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class enemyaduplic : MonoBehaviour
{
    private int hpmax = 30;
    private int hp = 30;
    public float spd;
    private EnemyPath Wpoints;

    private int waypointIndex;

    private int actualwayp;

    private bool way1=false;
    private bool way2=false;
    private bool way3=false;
    private bool fin=false;

    public Image hpbar;

    // Start is called before the first frame update
    void Start()
    {
        //InvokeRepeating("minvida",0,2f);
        Wpoints = GameObject.FindGameObjectWithTag("Waypoints").GetComponent<EnemyPath>();
    }

    // Update is called once per frame
    void Update()
    {
        hpbar.fillAmount =(float) ((hp*3f)/100f);
        if(hp<=0){
            Destroy(this.gameObject);
        }
        
        //comprobar que escena esta
        if(SceneManager.GetActiveScene().name=="Gameplay")
        //print("hola");
        //mientras queden waypoints por recorrer, movetowards
        //print("hola");
        if(Wpoints.waypoints.Length != waypointIndex)
        {
            //size hpbar
            if(!way1){
            waypointIndex = Random.Range(0,3);
            way1 = true;
            }

            transform.position = Vector2.MoveTowards(transform.position, Wpoints.waypoints[waypointIndex].position, spd * Time.deltaTime);

            if (Vector2.Distance(transform.position, Wpoints.waypoints[waypointIndex].position) < 0.1f)
            {
                if(!way2&&way1){
                    hp-=1;
                    waypointIndex = Random.Range(3,6);
                    way2 = true;
                }else if(!way3&&way2){
                    waypointIndex = Random.Range(6,9);
                    way3 = true;
                }else if(!fin){
                    waypointIndex=9;
                    fin = true;
                }else{
            
            waypointIndex=Wpoints.waypoints.Length;
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().minhp();
            Destroy(this.gameObject);
                }
                //waypointIndex++;
            }
        }
        
        

    }

    public void maxhpwaves(){
        hpmax+=15;
        hp=hpmax;
    }


    //prueba de perder vida y comprobar hpbar
        public void minvida(){
        //if(hp < hpmax){
        //}
        hp-=3;
        print(hp);
        print(hpbar.fillAmount);


    }

    public void RestarVida(int i)
    {
        this.hp -= i;
    }


}
