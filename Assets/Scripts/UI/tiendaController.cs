﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tiendaController : MonoBehaviour
{
    //Este script se encarga de activar el menu desplegable de la tienda.
    public GameObject menuDesplegable;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseUp()
    {
        if (!menuDesplegable.activeSelf)
        {
            menuDesplegable.SetActive(true);
        }
        else if (menuDesplegable.activeSelf)
         {
             menuDesplegable.SetActive(false);
         }
    }
}
