﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private int gamehp = 20;
    public int auxScore = 0;
    static public int score = 0;
    public int gold = 100;
    public int iron = 0;

    public enemya a;
    public enemyb b;

    public TextMeshProUGUI HP;
    public TextMeshProUGUI Score;
    public TextMeshProUGUI goldgui;
    public TextMeshProUGUI irongui;

    private int wave=0;
    private float cotime=15f;
    private float timer=0f;
    private GameObject myMenuTowerGround;
    private Button improveGroundButton;

    public AudioSource elevatorMusic;


    private void Awake()
    {
        elevatorMusic.Play();
        improveGroundButton = GetComponent<Button>();
    }

    //En esta función start inicializaremos algunos valores y obtendremos algunos GameObjects para luego operar con ellos.
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "Gameplay" || SceneManager.GetActiveScene().name == "Gameplay2")
        {
            HP.text = "HP: " + gamehp;
            goldgui.text = "" + gold;
            irongui.text = "" + iron;
            //InvokeRepeating("insene",0,2);
            myMenuTowerGround = GameObject.FindGameObjectWithTag("mejorarTorreSuelo");
            improveGroundButton = GameObject.Find("ImproveButtonTierra").GetComponent<Button>();
            myMenuTowerGround.SetActive(false);
            InvokeRepeating("passiveGold", 0, 1);
            //Instantiate(a);
        }   
    }

    // Update is called once per frame
    void Update()
    {
        if (!elevatorMusic.isPlaying)
        {
            elevatorMusic.Play();
        }


        if (SceneManager.GetActiveScene().name == "Gameplay" || SceneManager.GetActiveScene().name == "Gameplay2") {
            //la primera oleada es als 5 segons
            if (wave == 0)
                cotime = 10f;

            timer += Time.deltaTime;
            if (cotime < timer) {

                StartCoroutine("waves");
                a.SetSpd(a.GetSpd() + 0.02f);
                //cada 10 waves sube la hp de los enemigos
                if (wave % 10 == 0) {
                    a.maxhpwaves();
                    b.maxhpwaves();
                }
                //cada 5 waves spawnea enemigos voladores
                if (wave % 5 == 0)
                {
                    insenevol(); 
                    b.SetSpd(b.GetSpd() + 0.04f);
                }
                timer = timer - cotime;
            }
            minGold();
            minIron();
            if (Input.GetKeyDown(KeyCode.M))
            {
                wave++;
                print(wave);
            }

        }

        if(gamehp <= 0){
            SceneManager.LoadScene("GameOver",LoadSceneMode.Single);
        }

        minScore();
    }

    //En minHP reduciremos la vida actual de la base y actualizaremos el texto.
    public void minhp(){
        this.gamehp--;
        HP.text = "HP: " + gamehp;
    }

    //En minGold actualizaremos el texto de gold.
    public void minGold()
    {
        goldgui.text = "" + gold;
    }

    //passiveGold incrementará el oro progresivamente desde el InvokeRepeating del start.
    private void passiveGold()
    {
        gold += 5;
    }

    //En minIron actualizaremos el texto de iron.
    public void minIron()
    {
        irongui.text = "" + iron;
    }

    //En minScore actualizaremos el texto de score.
    public void minScore()
    {
        if (SceneManager.GetActiveScene().name == "Gameplay" || SceneManager.GetActiveScene().name == "Gameplay2")
        {
            score = auxScore;
        }
        Score.text = "Score: " + score;
    }

    public GameObject accessToImproveGround() {
        return myMenuTowerGround;
    }

    public Button accessToImproveButton()
    {
        return improveGroundButton;
    }

    // private void waves(){

    // }
    //instanciar enemy ground
    private void insene(){

        for (int i = 0; i < wave; i++)
        {
        Instantiate(a);
        //a.transform.position=this.transform.position;
        
        }
    }
    //instanciar enemy air
    private void insenevol()
    {

        for (int i = 0; i < wave; i++)
        {
            Instantiate(b);
            //a.transform.position=this.transform.position;

        }
    }

    //coroutine de les oleades
    IEnumerator waves(){
        wave++;
        // for (int i = 0; i < wave; i++)
        // {
        Invoke("insene",0);
        yield return new WaitForSeconds(2f);
        // }

    }


}
