using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Manager : MonoBehaviour
{
    // delegat
    private delegate void upgrade();
    private upgrade upgrade_lvl;

    private GameObject myManager;
    public GameObject Sprite;
    // nivell actual de la torre.
    private int Avtual_Lvl = 1;
    // num dispars de la torre
    private float Cadencia = 1;
    // enemic mor de 2 dispars
    private int dmg = 10;
    // enemic al que dispara
    public GameObject Enemy;
    // indica si la torre pot disparar o no
    private bool can_shoot = true;
    // altura de la torre per a instanciar la bala al disparar
    private float height;
    private GameObject target = null;
 
    private bool air = false;


    /// <summary>
    /// prepara el delegat de la torre i busca el game manager
    /// </summary>
    private void Start()
    {
        myManager = GameObject.FindGameObjectWithTag("GameManager");
        this.height = this.GetComponentInChildren<GroundChangeSprite>().GetSpriteHeight();
        this.upgrade_lvl +=  this.GetComponent<Manager>().lvlUp;
        this.upgrade_lvl +=  this.Sprite.GetComponent<GroundChangeSprite>().SpriteUpdate;
    }
    // Start is called before the first frame update

    private void Update()
    {
      
      /* if (Input.GetKeyDown(KeyCode.U))
        {
            print(this.Avtual_Lvl);
            this.Update_Tower(this.GetCanUpgrade());
        }*/
    }

    /// <summary>
    ///  retorna si la torreta es de tipus aeri
    /// </summary>
    /// <returns></returns>
    public bool GetIsAir ()
    {
        return this.air;
    }

    /// <summary>
    /// fa que la torreta sigui de tipus aeri
    /// </summary>
    public void SetIsAir()
    {
        this.air = true;
    }

    /// <summary>
    /// indica a la torreta cuan pot tornar a disparar
    /// </summary>
    public void PrepareNextShoot()
    {
        Invoke("ShootToTrue",this.Cadencia);
    }

     /// <summary>
    /// retorna el lvl actual de la torreta
    /// </summary>
    /// <returns></returns>
    public int GetActualLvl()
    {
        return this.Avtual_Lvl;
    }

    /// <summary>
    /// puja la torreta de nivell augmentant el dmg i la cadencia de dispar
    /// </summary>
    private void lvlUp()
    {
        this.Avtual_Lvl++;
        this.dmg += 2;
        this.Cadencia -= 0.2f;
        this.height = this.GetComponentInChildren<GroundChangeSprite>().GetSpriteHeight();
    }

    /// <summary>
    /// retorna si la torreta pot pujar-se mes de nivell
    /// </summary>
    /// <returns></returns>
    public Boolean GetCanUpgrade()
    {
        if (this.Avtual_Lvl == 5)
            return false;
        
        return true;
    }

    /// <summary>
    /// puja de nivell la torre i resta els recursos corresponent al jugador
    /// </summary>
    /// <param name="b"></param>
    public void Update_Tower(Boolean b)
    {
        if (this.Avtual_Lvl == 1 && myManager.GetComponent<GameManager>().gold >= 75) {
            if (b)
            {
                print("tower updated");
                myManager.GetComponent<GameManager>().gold -= 75;
                this.upgrade_lvl();
            }
        }
        else if (this.Avtual_Lvl == 2 && myManager.GetComponent<GameManager>().gold >= 100 && myManager.GetComponent<GameManager>().iron >= 20)
        {
            if (b)
            {
                print("tower updated");
                myManager.GetComponent<GameManager>().gold -= 100;
                myManager.GetComponent<GameManager>().iron -= 20;
                this.upgrade_lvl();
            }
        }
        else if (this.Avtual_Lvl == 3 && myManager.GetComponent<GameManager>().gold >= 150 && myManager.GetComponent<GameManager>().iron >= 40)
        {
            if (b)
            {
                print("tower updated");
                myManager.GetComponent<GameManager>().gold -= 150;
                myManager.GetComponent<GameManager>().iron -= 40;
                this.upgrade_lvl();
            }
        }
        else if (this.Avtual_Lvl == 4 && myManager.GetComponent<GameManager>().gold >= 230 && myManager.GetComponent<GameManager>().iron >= 70)
        {
            if (b)
            {
                print("tower updated");
                myManager.GetComponent<GameManager>().gold -= 230;
                myManager.GetComponent<GameManager>().iron -= 70;
                this.upgrade_lvl();
            }
        }
        else
        {
            myManager.GetComponent<GameManager>().goldgui.color = Color.red;
            myManager.GetComponent<GameManager>().irongui.color = Color.red;
            Invoke("returnColorGold", 1.5f);
            Invoke("returnColorIron", 1.5f);
        }
    }

    /// <summary>
    /// reseteja el color del or
    /// </summary>
    private void returnColorGold()
    {
        myManager.GetComponent<GameManager>().goldgui.color = Color.yellow;
    }

    /// <summary>
    /// reseteja el color del ferro
    /// </summary>
    private void returnColorIron()
    {
        myManager.GetComponent<GameManager>().irongui.color = Color.yellow;
    }

    /// <summary>
    /// retorna el dmg actual de la torre
    /// </summary>
    /// <returns></returns>
    public int GetDmg()
    {
        return this.dmg;
    }

    /// <summary>
    /// retorna el target de la torreta
    /// </summary>
    /// <returns>Game object</returns>
    public GameObject GetTarget()
    {
        return this.target;
    }

    /// <summary>
    /// assigna el target de la torreta
    /// </summary>
    /// <param name="go"></param>
    public void AddTarget(GameObject go)
    {
        this.target = go;
    }

    /// <summary>
    /// assigna el target2 de la torreta
    /// </summary>
    /// <param name="go"></param>
    public void AddTarget2(GameObject go)
    {
        this.target = go;
    }

    /// <summary>
    /// retorna la torreta
    /// </summary>
    /// <returns></returns>
    public GameObject GetTower()
    {
        return this.gameObject;
    }

    /// <summary>
    /// retorna el enemic (prefav)
    /// </summary>
    /// <returns></returns>
    public GameObject GetEnemy()
    {
        return this.Enemy;
    }

    /// <summary>
    /// indica que ja pot tornar a disparar
    /// </summary>
    private void ShootToTrue()
    {
      //  print("puc disparar");
      //  print(this.can_shoot);
        this.can_shoot = true;
    }

    /// <summary>
    /// fa que la torreta pugui tornar a disparar
    /// </summary>
    /// <returns></returns>
    public bool GetCanShoot()
    {
        return can_shoot;
    }

    /// <summary>
    /// indica que la torreta no pot disparar
    /// </summary>
    public void SetCanShootFalse()
    {
        this.can_shoot = false;
    }

    /// <summary>
    /// ajusta la alçada de la torreta
    /// </summary>
    /// <param name="f"></param>
    public void SetHeight(float f)
    {
        this.height = f;   
    }

    /// <summary>
    /// retorna la alçada de la torreta
    /// </summary>
    public float GetHeight()
    {
        return this.height;
    }
}
