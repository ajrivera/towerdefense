using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundChangeSprite : MonoBehaviour
{
    public Sprite[] Ground_sprites;

    // Start is called before the first frame update
    // Al crear el objecte fa que el sprite sigui

    public GameObject tower;

     /// <summary>
    /// assigna sprite a la torreta
    /// </summary>
    void Start()
    {
        this.GetComponent<SpriteRenderer>().sprite = Ground_sprites[0];
        this.transform.position = new Vector2(this.tower.transform.position.x, this.tower.transform.position.y + ((Ground_sprites[0].rect.height / 100) / 2)-0.22f);
        print((Ground_sprites[0].rect.height/100)/2);
        this.tower.GetComponent<Manager>().SetHeight((Ground_sprites[0].rect.height / 100));
    }

    /// <summary>
    /// canvia el sprite actual pel d'un nivell major
    /// </summary>
    public void SpriteUpdate()
    {
        this.GetComponent<SpriteRenderer>().sprite = Ground_sprites[this.GetComponentInParent<Manager>().GetActualLvl() - 1];
        this.transform.position = new Vector2(this.tower.transform.position.x, this.tower.transform.position.y + ((Ground_sprites[this.GetComponentInParent<Manager>().GetActualLvl() - 1].rect.height / 100) / 2) - 0.22f);
    }

    /// <summary>
    /// retorna la alçada del sprite assignat a la torreta
    /// </summary>
    /// <returns></returns>
    public float GetSpriteHeight()
    {
        return (Ground_sprites[this.GetComponentInParent<Manager>().GetActualLvl() - 1].rect.height) / 100;
    }
}
