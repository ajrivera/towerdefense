﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PutTower : MonoBehaviour
{
    //Este script se encarga de poner las torres en el lugar indicado.

    public Button buyGroundTower;
    public Button buyFlyingTower;
    public Texture2D towerGroundSprite;
    public Texture2D towerFlyingSprite;
    public Texture2D defaultMouse;
    public GameObject groundTower;
    public GameObject FlyingTower;
    public Tilemap tmNoEdificable;
    public Tile noEdifificable;
    private GameObject myManager;

    private Vector3Int gridPosition;

    private bool situandoTorreGround = false;
    private bool situandoTorreAir = false;

    //En el start ininiciaremos los listeners de los botones y el sprite del cursor.
    void Start()
    {
        myManager = GameObject.FindGameObjectWithTag("GameManager");
        Cursor.SetCursor(defaultMouse, Vector2.zero, CursorMode.ForceSoftware);
        if (SceneManager.GetActiveScene().name == "Gameplay" || SceneManager.GetActiveScene().name == "Gameplay2")
        {
            buyGroundTower.onClick.AddListener(onClickGround);
            buyFlyingTower.onClick.AddListener(onClickFlying);
        }
    }

    //En este update estaremos constantemente comprobando si estamos intentando posicionar una torre y haremos el proceso en caso de que si.
    void Update()
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (situandoTorreGround)
        {
            if (Input.GetButton("Fire1")){
                gridPosition = tmNoEdificable.WorldToCell(mousePosition);
                if (!tmNoEdificable.HasTile(gridPosition)) 
                {
                    Instantiate(groundTower, mousePosition, Quaternion.identity);
                    tmNoEdificable.SetTile(gridPosition, noEdifificable);
                    Cursor.SetCursor(defaultMouse, Vector2.zero, CursorMode.ForceSoftware);
                    situandoTorreGround = false;
                }
            }
                
        }
        else if (situandoTorreAir)
        {
            if (Input.GetButton("Fire1"))
            {
                gridPosition = tmNoEdificable.WorldToCell(mousePosition);
                if (!tmNoEdificable.HasTile(gridPosition))
                {
                    Instantiate(FlyingTower, mousePosition, Quaternion.identity);
                    tmNoEdificable.SetTile(gridPosition, noEdifificable);
                    Cursor.SetCursor(defaultMouse, Vector2.zero, CursorMode.ForceSoftware);
                    situandoTorreAir = false;
                }
            }

        }
    }

    //Comprobaremos si hemos pulsado el botón de comprar torre del suelo.
    void onClickGround()
    {
        if (myManager.GetComponent<GameManager>().gold >= 100)
        {
            situandoTorreGround = true;
            myManager.GetComponent<GameManager>().gold -= 100;
            Cursor.SetCursor(towerGroundSprite, Vector2.zero, CursorMode.ForceSoftware);

        }
        else {
            myManager.GetComponent<GameManager>().goldgui.color = Color.red;
            Invoke("returnColorGold", 1.5f);
        }
    }

    //Comprobaremos si hemos pulsado el botón de comprar torre voladora.
    void onClickFlying()
    {
        if (myManager.GetComponent<GameManager>().gold >= 150)
        {
            situandoTorreAir = true;
            myManager.GetComponent<GameManager>().gold -= 150;
            Cursor.SetCursor(towerGroundSprite, Vector2.zero, CursorMode.ForceSoftware);

        }
        else
        {
            myManager.GetComponent<GameManager>().goldgui.color = Color.red;
            Invoke("returnColorGold", 1.5f);
        }

    }

    void returnColorGold() {
        myManager.GetComponent<GameManager>().goldgui.color = Color.yellow;
    }
}
