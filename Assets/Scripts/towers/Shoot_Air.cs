using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

public class Shoot_Air : MonoBehaviour
{
    public GameObject Bala;
    private GameObject Enemy;


    // Start is called before the first frame update

    /// <summary>
    /// assigna enemic a la torreta i indica que aquesta es de tipo aeri
    /// </summary>
    void Start()
    {
        //this.Enemy = ;
        this.Enemy = this.GetComponentInParent<Manager>().GetEnemy();
        print(this.Enemy.transform.tag);
        print(this.GetComponentInParent<Manager>().GetEnemy().transform.tag);
        this.GetComponentInParent<Manager>().SetIsAir();
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// assigna target a la torreta si aquest en null
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == this.Enemy.transform.tag)
        {
            // assigna el target1
            if (this.GetComponentInParent<Manager>().GetTarget() == null
                && collision.GetComponent<enemyb>().GetHp() > 0)
            {
                this.GetComponentInParent<Manager>().AddTarget(collision.gameObject);
                //print("enemic assignat");
            }
        }

    }

    /// <summary>
    /// assigna target si es null, el borra si esta morint i li dispara si esta viu
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == this.Enemy.transform.tag)
        {
            // assigna el target
            if (this.GetComponentInParent<Manager>().GetTarget() == null
                && collision.GetComponent<enemyb>().GetHp() > 0)
            {
                this.GetComponentInParent<Manager>().AddTarget(collision.gameObject);
                //print("enemic assignat2");
            }
        }
        // borra target si aquest esta morint
        if (this.GetComponentInParent<Manager>().GetTarget() != null && this.GetComponentInParent<Manager>().GetTarget().GetComponent<enemyb>().GetHp() <= 0)
        {
            
            this.GetComponentInParent<Manager>().AddTarget(null);
        }
        if (this.GetComponentInParent<Manager>().GetCanShoot() && collision.transform.tag == this.Enemy.transform.tag)
        {
            
            this.GetComponentInParent<Manager>().PrepareNextShoot();
            // Dispara al target
            Instantiate(Bala, new Vector3(this.GetComponentInParent<Transform>().position.x, this.GetComponentInParent<Transform>().position.y + this.GetComponentInParent<Manager>().GetHeight(),
                -1), Quaternion.identity).GetComponent<FollowEnemy>().SetTarget(this.GetComponentInParent<Manager>().GetTarget(), this.GetComponentInParent<Manager>().GetTower());
            this.GetComponentInParent<Manager>().SetCanShootFalse();
           

        }
    }




    /// <summary>
   /// borrra el target si aquest surt del rang de dispar
   /// </summary>
   /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (this.GetComponentInParent<Manager>().GetTarget() != null && collision.gameObject == this.GetComponentInParent<Manager>().GetTarget())
        {
            this.GetComponentInParent<Manager>().AddTarget(null);
        }
    }

}