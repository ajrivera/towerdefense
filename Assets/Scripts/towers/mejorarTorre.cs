﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mejorarTorre : MonoBehaviour
{
    GameObject gameManager;
    GameObject menuImproveTowerGround;
    private Button _Button;



    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
        menuImproveTowerGround =  gameManager.GetComponent<GameManager>().accessToImproveGround();
        _Button = gameManager.GetComponent<GameManager>().accessToImproveButton();
        _Button.onClick.AddListener(onClickGround);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseUp()
    {
        if (!menuImproveTowerGround.activeSelf)
        {
            menuImproveTowerGround.SetActive(true);
            _Button.GetComponent<myTowerVariable>().myTransform = this.transform;
        }
        else if (menuImproveTowerGround.activeSelf)
        {
            menuImproveTowerGround.SetActive(false);
        }
    }

    private void onClickGround() {
        if(this.transform == _Button.GetComponent<myTowerVariable>().myTransform)
        {
            this.GetComponent<Manager>().Update_Tower(this.GetComponent<Manager>().GetCanUpgrade());
        }
    }
}


